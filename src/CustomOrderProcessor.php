<?php
 
namespace Drupal\commerce_suretax;
 
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_order\Adjustment;
 
/**
* Provides an order processor that modifies the cart according to the business logic.
*/
class CustomOrderProcessor implements OrderProcessorInterface
{
 /**
  * {@inheritdoc}
  */
 public function process(OrderInterface $order)  {
   foreach ($order->getItems() as $order_item) {
     // SetAdjustment to empty initially.
     $order_item->setAdjustments([]);
     $product_variation = $order_item->getPurchasedEntity();
     if (!empty($product_variation)) {
      
         $adjustments = $order_item->getAdjustments();
         // Apply custom adjustment.
         $adjustments[] = new Adjustment([
           'type' => 'tax',
           'label' => 'Discounted Price - ' . $product_title,
           'amount' => new Price('-' . 40, 'USD'),
         ]);
         $order_item->setAdjustments($adjustments);
         $order_item->save();
       }
     }
  }
}