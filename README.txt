INTRODUCTION
============
The Drupal Commerce Connector for Commerce Suretax is a Drupal compliant module that
integrates the Drupal Commerce check-out process with Commerce Suretax and is used for sales tax calculations.

The module supports two modes - Development(for testing purpose) and Live(RealTime transactions).

REQUIREMENTS
============
a) The service uses the Commerce SureTax ReST api for processing transactions.
b) The server PHP configuration must support cURL

NEW INSTALLATION
=================
Installing the module is done as for any custom Drupal Commerce module

a) Unzip & copy the folder "commerce_suretax" to the location shown below,
or in accordance with your Drupal Commerce configuration.

YOURSITE directory modules/commerce_suretax or modules/custom/commerce_suretax

b) Enable the module (Commerce SureTax) in the usual way.
c) After successful installation a commerce line item type and product variation will be created.

CONFIGURATION
=============
Select Store -> Configuration -> Commerce Suretax (YOURSITE/admin/commerce/config/commerce_suretax)

Complete the information requested, as is applicable to edition selected.
Save the form - Commerce Suretax settings - on completion.

GENERAL
=======
-> Select Mode(Development/Live).
-> Enter all details like clientId, ValidationKey, Commerce Suretax Post and Cancel API's

WORKING
=======
-> When User add a products and do checkout then Commerce SureTax Will calculate and adds a commerce suretax orderitem to order and also saves log in watchdog.

-> Manual creation of Order will give Commerce suretax lineitem only if there is product in order page.
