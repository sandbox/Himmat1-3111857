<?php

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Logger\RfcLoggerTrait;
use Psr\Log\LoggerInterface;

/**
 * Creates SureTax Sales Calculation / Deletion based on request.
 * 
 * @param type $order
 *   Complete Order Details.
 * @param type $order_item
 *   OrderItem Details.
 * @param type $uid
 *   uid of User that created Order.
 * @param type $request
 *   Request that has POST / CANCEL process.
 */
function commercesuretax_sales_request_tax($order_load) {
  // Fetch Suretax Details.
  $suretax_config = \Drupal::config('commerce.suretax.settings');
  $mode = $suretax_config->get('suretax_mode');
  $ClientNumber = $suretax_config->get('suretax_client_id_' . $mode);
  $ValidationKey = $suretax_config->get('suretax_validation_key_' . $mode);

  /* fetch billing address */
  $payment_method = $order_load->get('payment_method')->first()->entity;
  $billing_address = $payment_method->getBillingProfile()->get('address')->first();

  $order_items = $order_load->getItems();

  $tax_total = 0;
  $complete_array = '';
  foreach ($order_items as $order_item) {
     $product_variation = $order_item->getPurchasedEntity();
     if($product_variation->type->target_id == 'default') {
      // Calculate only product price for Order Total.
      $tax_total += ($order_item->unit_price->number * $order_item->quantity->value); 
       $tax_total = round($tax_total);
      $array = '{
        "LineNumber":  "'.$order_load->uuid().'",
        "InvoiceNumber": "'.$order_item->order_id->value.'",
        "CustomerNumber": "'.$order_load->getCustomerId().'",
        "TransDate": "'.date("Y-m-d").'",
        "Revenue": "'. $tax_total .'",
        "TaxIncludedCode": "0",
        "Units": "1",
        "TaxSitusRule": "22",
        "TransTypeCode": "990101",
        "SalesTypeCode": "R",
        "RegulatoryCode": "70",
        "TaxExemptionCodeList": "",
        "ExemptReasonCode": "",
        "UDF": "",
        "UDF2": "",
        "FreightOnBoard": "",
        "ShipFromPOB": "false",
        "MailOrder": "false",
        "CommonCarrier": "false",
        "AuxRevenue": "0",
        "AuxRevenueType": "",
        "GLAccount": "",
        "MaterialGroup": "",
        "RuleOverride": "",
        "CurrencyCode": "'.$order_load->total_price->currency_code.'",
        "ShipToAddress": {
            "PrimaryAddressLine": "'.$billing_address->getAddressLine1().'",
            "SecondaryAddressLine": "'.$billing_address->getAddressLine2().'",
            "County": "'.$billing_address->getCountryCode().'",
            "City": "'.$billing_address->getLocality().'",
            "State": "'.$billing_address->getAdministrativeArea().'",
            "PostalCode": "'.$billing_address->getPostalCode().'",
            "Plus4": "0000",
            "VerifyAddress":"false"
        },
       "ShipFromAddress": {
           "PrimaryAddressLine": "'.$billing_address->getAddressLine1().'",
            "SecondaryAddressLine": "'.$billing_address->getAddressLine2().'",
            "County": "'.$billing_address->getCountryCode().'",
            "City": "'.$billing_address->getLocality().'",
            "State": "'.$billing_address->getAdministrativeArea().'",
            "PostalCode": "'.$billing_address->getPostalCode().'",
            "Plus4": "0000",
            "VerifyAddress":"false"
      },

    }';
      $complete_array .= $array . ',';
    }
  }
  $stan = $suretax_config->get('suretax_stan');
  // Array of OrderItems.
  $complete_array = rtrim($complete_array, ',');

  // JSON Request for Suretax
  $get_request = '{

"ClientNumber":"' . $ClientNumber . '",
"ValidationKey":"' . $ValidationKey . '",
"DataYear":"'.date("Y").'",
"DataMonth":"'.date("m").'",
"TotalRevenue":"' . $tax_total . '",
"ReturnFileCode":"0",
"ClientTracking":"test",
"IndustryExemption":"",
"ResponseType":"D",
"ResponseGroup":"03",
"STAN": "' . $stan . '",
"ItemList":[
    ' . $complete_array . '
    ]}';
  $api_url = $suretax_config->get('suretax_api_' . $mode);

  // Initialize curl to post data.
  $api_session = curl_init();
  //echo "<pre>"; echo  $get_request; die;
  curl_setopt($api_session, CURLOPT_HTTPHEADER, array('Accept-Encoding: gzipped'));
  curl_setopt($api_session, CURLOPT_ENCODING, 'gzip');
  $post_string = gzencode("request=$get_request");
  curl_setopt($api_session, CURLOPT_POSTFIELDS, $post_string);
  curl_setopt($api_session, CURLOPT_POST, 1);
  curl_setopt($api_session, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($api_session, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($api_session, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($api_session, CURLOPT_URL, $api_url);
  curl_setopt($api_session, CURLOPT_TIMEOUT, 20);
  $response = curl_exec($api_session);
  $curl_data = str_replace('<?xml version="1.0" encoding="utf-8"?>', '', $response);
  $curl_data = str_replace('<string xmlns="http://tempuri.org/">', '', $curl_data);
  $curl_data = str_replace('</string>', '', $curl_data);
  curl_close($api_session);
  $result = json_decode($curl_data, TRUE);

  if ($result) {
    // Insert results into Suretax table.
    db_merge('commerce_suretax')
        ->key(array(
          'user_id' => $order_load->getCustomerId(),
          'transaction_id' => $result['TransId'],
          'success' => 'Yes',
          'order_id' => $order_load->getOrderNumber(),
        ))
        ->fields(array(
          'user_id' => $order_load->getCustomerId(),
          'transaction_id' => $result['TransId'],
          'total_tax' => $result['TotalTax'],
          'response_code' => $result['ResponseCode'],
          'success' => $result['Successful'],
          'api_response' => $curl_data,
          'order_id' => $order_load->getOrderNumber(),
        ))
        ->execute();
  }
  \Drupal::logger('suretax')->notice('<pre>' . print_r($result, true) . '</pre>');
 
  // If response is success then return suretax total. 
  if ($result['Successful'] == 'Y') {
    return $result['TotalTax'];
  }
  else {
    return FALSE;
  }
}

/**
 * Implements suretax delete for order..
 */
function commercesuretax_delete_tax($order_id) {
  // Get last transaction ID for Order.
  $query = db_select('commerce_suretax', 's');
  $query->fields('s', array('transaction_id'));
  $query->condition('order_id', $order_id, '=');
  $query->orderby('transaction_id', 'DESC')->range(0, 1);
  $TransId = $query->execute()->fetchField();
  // Get Suretax Details.
  $mode = $suretax_config->get('suretax_mode');
  $ClientNumber = $suretax_config->get('suretax_client_id_' . $mode);
  $ValidationKey = $suretax_config->get('suretax_validation_key_' . $mode);
  // Post fields for Cancel Order Suretax.
  $post_fields = '{
    "ClientNumber":"' . $ClientNumber . '",
    "ClientTracking":"test",
    "TransId":"' . $TransId . '",
    "ValidationKey":"' . $ValidationKey . '"
  }';
//Initiate cURL request.
  $ch = curl_init();
// SureTax Cancel request URL.
  $suretax_url = $suretax_config->get('suretax_cancel_api_' . $mode);
// Set Headers.
  curl_setopt($ch, CURLOPT_POSTFIELDS, "requestCancel=$post_fields"); // Assign POST Data
  curl_setopt($ch, CURLOPT_POST, 1); // Select POST as transfer type
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_URL, $suretax_url); // The Web service URL
  curl_setopt($ch, CURLOPT_TIMEOUT, 20); // Set timeout
  $response = curl_getinfo($ch);
  // Close curl.
  curl_close($ch);
}

/**
 * Implements for suretax delete transaction.
 */
function commercesuretax_request_tax_delete($order_id) {
  // Delete transactions in suretax table.
  if ($order_id) {
    db_delete('commerce_suretax')
        ->condition('order_id', $order_id, '=')
        ->execute();
  }
}
